/*
@author: Dewei Chen 
@date: 2-17-2012
@class: CIS27A
@instructor: Dave Harden
@filename: a6_1.java
@description: This program draws a rocket on the screen
based on 3 user inputted parameters: rocket stage width,
rocket stage height, and how many stages in the rocket.
There will be a rocket tip and exhaust end in the form 
of a cone shape at the top and bottom of the rocket. The
cone shape changes based on the stage width. The design
of the program is broken down into simpler methods.
*/

import java.util.Scanner;

public class a6_1 {
	
	//Main method gets user parameters input and calls drawRocket
	public static void main(String[] args) {
		
		Scanner input = new Scanner(System.in);
		int width;
		int height;
		int numStages;
		
		System.out.print("Enter the width of each stage: ");
		width = input.nextInt();
		System.out.print("Enter the height of each stage: ");
		height = input.nextInt();
		System.out.print("Enter how many stages in the rocket: ");
		numStages = input.nextInt();

		drawRocket(width,height,numStages);
		
	}
	
	//drawRocket method passes the 3 params and calls other methods to draw a rocket
	public static void drawRocket(int width, int height, int numStages){
		
		drawCone(width);    
		for (int i=1 ; i<=numStages ; i++) 
			drawBox(width,height);
        drawCone(width);	
		
	}
	
	//drawCone method calls other methods based on if the width is even or odd
	public static void drawCone(int width) {
		
		//Cone shape is different for even and odd widths
		if ((width % 2) == 0) {
			for (int j=1 ; j<=width/2 ; j++)
				drawConeRowEven(width,j);
		}
		else {
			for (int j=1 ; j<=width/2+1 ; j++)
				drawConeRowOdd(width,j);
		}
		
	}
	
	//drawConeRowEven draws a cone row of * if width is even
	public static void drawConeRowEven(int width, int rowNum) {
		
		for (int s1=1 ; s1<=(width/2-rowNum) ; s1++)
			System.out.print(" ");
		System.out.print("*");
		for (int s2=1 ; s2<=(rowNum-1)*2 ; s2++)
			System.out.print(" ");
		System.out.print("*\n");
		
	}
	
	//drawConeRowOdd draws a cone row of * if width is odd
	public static void drawConeRowOdd(int width, int rowNum) {
		
		//First row of odd cone has only 1 * instead of the usual 2
		if (rowNum == 1) {
			for (int s1=1 ; s1<=(width/2-rowNum+1) ; s1++)
				System.out.print(" ");
			System.out.print("*\n");
		}
		else {
			for (int s1=1 ; s1<=(width/2-rowNum+1) ; s1++)
				System.out.print(" ");
			System.out.print("*");
			for (int s2=1 ; s2<=(2*(rowNum-2)+1) ; s2++)
				System.out.print(" ");
			System.out.print("*\n");
		}
		
	}
	
	//drawBox method calls 2 other methods to draw a box
	public static void drawBox(int width, int height) {
		
		drawHorizontalLine(width);
		draw2VerticalLines(width, height);
		drawHorizontalLine(width);
		
	}
	
	//draw2VerticalLines method draw 2 vertical lines of * based on width and height
	public static void draw2VerticalLines(int width, int height) {
		
		for (int j=1 ; j<=height-2 ; j++)
			drawOneRow(width);
		
	}
	
	//drawHorizontalLine method draws 1 horizontal line of * based on width
	public static void drawHorizontalLine(int width) {
		
		for (int i=1 ; i<=width ; i++)
			System.out.print("*");
		System.out.print("\n");
		
	}
	
	//drawOneRow method draws a row with 2 * separated by width distance
	public static void drawOneRow(int width) {
		
		System.out.print("*");
		for (int i=2 ; i<width ; i++)
			System.out.print(" ");
		System.out.print("*\n");
		
	}

}
